import { createStackNavigator, createAppContainer } from "react-navigation";
import Start from "./components/Start";
import Main from "./components/Main";
import CameraScreen from "./components/CameraScreen";
import Photo from "./components/Photo"

const Root = createStackNavigator({
  start: { screen: Start },
  main: { screen: Main },
  camera: { screen: CameraScreen },
  photo: { screen: Photo },
});

const App = createAppContainer(Root);

export default App;