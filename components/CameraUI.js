import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Image } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { ScreenOrientation } from 'expo';

class CameraUI extends Component {
    constructor(props) {
        super(props);
        this.state = {
            screen: Dimensions.get('window'),
        };
        this.getOrientation = this.getOrientation.bind(this)
        this.getStyle = this.getStyle.bind(this);
        this.onLayout = this.onLayout.bind(this);
    }
    klik() {

    }
    render() {
        return (
            <View style={this.getStyle().container} onLayout={this.onLayout}>
                <TouchableHighlight underlayColor="rgba(100,100,100,0.4)" style={this.getStyle().smallButton} onPress={this.props.changeCamera}>
                    <Image style={{ width: 50, height: 50 }} source={require('../assets/swap.png')} />
                </TouchableHighlight>
                <TouchableHighlight underlayColor="rgba(100,100,100,0.4)" style={styles.bigButton} onPress={this.props.takePicture}></TouchableHighlight>
                <TouchableHighlight underlayColor="rgba(100,100,100,0.4)" style={this.getStyle().smallButton} onPress={this.props.sett}>
                    <Image style={{ width: 50, height: 50 }} source={require('../assets/settings.png')} />
                </TouchableHighlight>
            </ View>
        );
    }
    getOrientation() {
        if (this.state.screen.width > this.state.screen.height) {
            return 'LANDSCAPE';
        } else {
            return 'PORTRAIT';
        }
    }
    getStyle() {
        if (this.getOrientation() === 'LANDSCAPE') {
            return landscapeStyles;
        } else {
            return portraitStyles;
        }
    }
    async onLayout() {
        this.setState({ screen: Dimensions.get('window') });
    }
}
const styles = StyleSheet.create({

    bigButton: {
        margin: 10,
        height: 100,
        width: 100,  //The Width must be the same as the height
        borderRadius: 200, //Then Make the Border Radius twice the size of width or Height
        borderColor: '#bbbbbb',
        borderWidth: 2,
    }
});

const portraitStyles = StyleSheet.create({
    container: {
        flex: 1, justifyContent: "space-evenly", alignItems: "flex-end", flexDirection: "row"
    },
    smallButton: {
        margin: 5,
        marginBottom: 20,
        height: 50,
        width: 50,  //The Width must be the same as the height
        borderRadius: 100, //Then Make the Border Radius twice the size of width or Height
    }
});

const landscapeStyles = StyleSheet.create({
    container: {
        flex: 1, justifyContent: "center", alignItems: "flex-end", flexDirection: "column-reverse"
    },
    smallButton: {
        margin: 10,
        marginRight: 20,
        height: 50,
        width: 50,  //The Width must be the same as the height
        borderRadius: 100, //Then Make the Border Radius twice the size of width or Height
    }
});
export default CameraUI;
