import React, { Component } from 'react';
import { View, ScrollView, Text } from 'react-native';
import * as Permissions from "expo-permissions";
import PhotoItem from './PhotoItem';

class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
        this.loadPhotos = this.loadPhotos.bind(this);
    }
    async componentWillMount() {
        let { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('brak uprawnień do czytania image-ów z galerii')
        }
    }
    async componentDidMount() {
        await this.loadPhotos();
    }
    async loadPhotos() {
        this.props.refreshPhotos();
        //     let obj = await MediaLibrary.getAssetsAsync({
        //         first: 100,           // ilość pobranych assetów
        //         mediaType: 'photo',
        //         sortBy: 'creationTime'   // typ pobieranych danych, photo jest domyślne
        //     })
        //     // obj.assets.sort((a, b) => (a.creationTime < b.creationTime) ? 1 : -1)

        //     this.setState({
        //         data: obj.assets
        //     })
        //     // alert(JSON.stringify(this.state.data, null, 4))
    }
    render() {
        var tab = this.props.data.map(item => {
            return <PhotoItem key={item.id} data={item} styles={this.props.styles} click={this.props.click} longClick={this.props.longClick} />
        })
        return (
            <ScrollView>
                <View style={{ flex: 1, flexDirection: "row", flexWrap: "wrap" }}>
                    {tab}
                </View>
            </ScrollView>
        );
    }
}

export default Gallery;
