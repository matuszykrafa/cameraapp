import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import * as Font from "expo-font";
import MyButton from './MyButton';

class Start extends Component {
    static navigationOptions = {
        header: null,
    }
    constructor(props) {
        super(props);
        this.state = {
            fontloaded: false
        }
        this.handleStart = this.handleStart.bind(this)
    }
    componentWillMount = async () => {
        await Font.loadAsync({
            'myfont': require('../assets/Amatic-Bold.ttf'),
        });
        this.setState({ fontloaded: true })
    }
    handleStart() {
        this.props.navigation.navigate("main")
    }
    render() {
        return (
            this.state.fontloaded
                ?
                <React.Fragment>
                    <View style={{ flex: 1, backgroundColor: "#1b5e20", justifyContent: "center", alignItems: "center" }}>
                        <Text style={{ fontSize: 80, color: "#eeeeee", fontFamily: 'myfont' }}> CameraApp </Text>
                        <Text style={{ fontSize: 36, color: "#eeeeee", fontFamily: 'myfont' }} > Aplikacja do zarządzania zdjęciami </Text>
                        <View style={{ width: "90%" }}><MyButton width="100%" text="Start" click={this.handleStart} /></View>
                    </View>
                </React.Fragment>
                :
                <React.Fragment>
                    <View style={{ flex: 1, backgroundColor: "#1b5e20", justifyContent: "center", alignItems: "center" }}>
                        <Text style={{ fontSize: 64, color: "#eeeeee" }}> CameraApp </Text>
                        <Text style={{ fontSize: 24, color: "#eeeeee" }} > Aplikacja do zarządzania zdjęciami</Text>
                        <MyButton width="100%" text="Start" click={this.handleStart} />
                    </View>
                </React.Fragment>



        );
    }
}
export default Start;
