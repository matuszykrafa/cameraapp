import React, { Component } from 'react';
import { View, Text, Dimensions, Animated, ToastAndroid, BackHandler, StyleSheet } from 'react-native';
import * as Permissions from "expo-permissions";
import { Camera } from 'expo-camera';
import CameraUI from './CameraUI'
import * as MediaLibrary from "expo-media-library";
import CameraSettings from './CameraSettings';
import RadioGroup from './RadioGroup';
import { ScreenOrientation } from 'expo';

class CameraScreen extends Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.offset = -30
        this.state = {
            ratios: ["4:3", "16:9"],
            whiteBalances: [],
            flashModes: [],
            resolutions: [],
            ratio: "16:9",
            whiteBalance: "auto",
            flashMode: "off",
            resolution: "2048x1536",
            hasCameraPermission: null,
            type: Camera.Constants.Type.back,
            pos: new Animated.Value(Dimensions.get("window").height - this.offset),
        };
        this.isHidden = true
        this.takePicture = this.takePicture.bind(this);
        this.changeCamera = this.changeCamera.bind(this);
        this.settingsAnim = this.settingsAnim.bind(this);
        this.getSizes = this.getSizes.bind(this);
        this.changeBalance = this.changeBalance.bind(this);
        this.changeFlash = this.changeFlash.bind(this);
        this.changeResolution = this.changeResolution.bind(this);
        this.changeRatio = this.changeRatio.bind(this);

        this.styles = StyleSheet.create({
            animatedView: {
                position: "absolute",
                left: 0,
                top: 0,
                backgroundColor: "#1c1c1caa",
                width: Dimensions.get("window").width * (2 / 3),
                height: Dimensions.get("window").height - this.offset,
            }
        });
    }

    async settingsAnim(ref) {
        await this.getSizes(ref)
        if (this.isHidden)
            toPos = 0;
        else
            toPos = Dimensions.get("window").height - this.offset
        Animated.spring(
            this.state.pos,
            {
                toValue: toPos,
                velocity: 1,
                tension: 0,
                friction: 10,
            }
        ).start();
        this.isHidden = !this.isHidden;
    }

    async getSizes(camera) {
        if (camera) {
            if (this.state.resolutions.length == 0) {
                const sizes = await camera.getAvailablePictureSizesAsync(this.state.ratios[0])
                this.setState({
                    resolutions: sizes
                })
            }
        }
    };

    getWhiteBalances() {
        if (this.state.whiteBalances.length == 0) {
            let whiteBalance = Camera.Constants.WhiteBalance;
            let whiteArray = [];
            for (var elem in whiteBalance) {
                whiteArray.push(elem);
            }
            this.setState({
                whiteBalances: whiteArray
            })
        }
    }

    getFlashMode() {
        if (this.state.flashModes.length == 0) {
            let flashMode = Camera.Constants.FlashMode;
            let flashArray = [];
            for (var elem in flashMode) {
                flashArray.push(elem);
            }
            this.setState({
                flashModes: flashArray
            })
        }
    }

    async componentWillMount() {
        await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP)
        this.getWhiteBalances();
        this.getFlashMode();

        let { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status == 'granted' });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    componentWillUnmount() {
        ScreenOrientation.unlockAsync();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    handleBackPress = () => {
        this.props.navigation.state.params.refreshPhotos();
        this.props.navigation.goBack()
        return true;
    }
    async takePicture() {
        if (this.camera) {
            let foto = await this.camera.takePictureAsync();
            let asset = await MediaLibrary.createAssetAsync(foto.uri); // domyslnie zapisuje w DCIM
            ToastAndroid.showWithGravity(
                'Zdjęcie zrobione',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
            );
            // alert(JSON.stringify(asset, null, 4))
        }
    }
    changeCamera() {
        this.setState({
            type: this.state.type === Camera.Constants.Type.back
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back,
        });
    }
    changeBalance(value) {
        this.setState({
            whiteBalance: value
        })
    }

    changeFlash(value) {
        this.setState({
            flashMode: value
        })
    }

    changeResolution(value) {
        this.setState({
            resolution: value
        })
    }

    changeRatio(value) {
        this.setState({
            ratio: value
        })
    }
    render() {
        const { hasCameraPermission } = this.state; // podstawienie zmiennej ze state
        if (hasCameraPermission == null) {
            return <View />;
        } else if (hasCameraPermission == false) {
            return <Text>brak dostępu do kamery</Text>;
        } else {
            return (
                <View style={{ flex: 1, backgroundColor: '#000000' }}>
                    <View style={{ flex: 1 }}>
                        {/* <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').width * 4 / 3 }}> */}
                        <Camera
                            ref={ref => {
                                this.camera = ref; // Uwaga: referencja do kamery używana później
                            }}
                            style={{ flex: 1 }}
                            type={this.state.type}
                            whiteBalance={this.state.whiteBalance}
                            flashMode={this.state.flashMode}
                            ratio={this.state.ratio}
                            pictureSize={this.state.resolution}>
                            <Animated.ScrollView
                                style={[this.styles.animatedView, { transform: [{ translateY: this.state.pos }] }]} >
                                <Text style={{ padding: 20, fontSize: 20, color: "white", marginTop: 10 }}>Settings</Text>
                                <RadioGroup groupName="White balance" radioTab={this.state.whiteBalances} startValue={this.state.whiteBalance} onchange={this.changeBalance} />
                                <RadioGroup groupName="Flash mode" radioTab={this.state.flashModes} startValue={this.state.flashMode} onchange={this.changeFlash} />
                                <RadioGroup groupName="Camera ratio" radioTab={this.state.ratios} startValue={this.state.ratio} onchange={this.changeRatio} />
                                <RadioGroup groupName="Picture sizes" radioTab={this.state.resolutions} startValue={this.state.resolution} onchange={this.changeResolution} />
                            </Animated.ScrollView>
                            <CameraUI takePicture={this.takePicture} changeCamera={this.changeCamera} sett={() => { this.settingsAnim(this.camera) }} />
                        </Camera>
                        {/* </View> */}
                        {/* <View style={{ flex: 1 }}>
                            
                        </View> */}
                    </View>
                </View>
            );
        }
    }
}

export default CameraScreen;
