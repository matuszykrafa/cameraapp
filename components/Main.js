import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, ToolbarAndroid } from 'react-native';
import MyButton from './MyButton';
import Gallery from './Gallery'
import * as MediaLibrary from "expo-media-library";
import { Header } from 'react-navigation';


class Main extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: "Galeria zdjęć",
        headerStyle: {
            backgroundColor: "#1b5e20",
        },
        headerTitleStyle: {
            color: "#eeeeee"
        },
        headerTintColor: '#eeeeee',
        headerRight: <ToolbarAndroid
            style={{
                backgroundColor: '#1b5e20',
                height: 56, width: 56,
                elevation: 0 // cień poniżej
            }}

            actions={[
                { title: 'Siatka/Lista', show: 'never' },
                { title: 'Kamera', show: 'never' },
                { title: 'Usuń zaznaczone', show: 'never' }
            ]}
            onActionSelected={navigation.getParam('onActionSelected')}
        />
    })
    constructor(props) {
        super(props);
        this.state = {
            screen: Dimensions.get('window'),
            grid: true,
            data: [],
            selected: []
        };
        this.onActionSelected = this.onActionSelected.bind(this);
        this.openCamera = this.openCamera.bind(this);
        this.changeGrid = this.changeGrid.bind(this);
        this.getGrid = this.getGrid.bind(this);
        this.goToPhoto = this.goToPhoto.bind(this);
        this.longClick = this.longClick.bind(this);
        this.refreshPhotos = this.refreshPhotos.bind(this);
        this.deleteSelected = this.deleteSelected.bind(this);

        this.getOrientation = this.getOrientation.bind(this)
        this.getStyle = this.getStyle.bind(this);
        this.onLayout = this.onLayout.bind(this);
    }
    componentDidMount() {
        this.props.navigation.setParams({ onActionSelected: this.onActionSelected })
    }
    onActionSelected(e) {
        switch (e) {
            case 0:
                this.changeGrid()
                break;
            case 1:
                this.openCamera()
                break;
            case 2:
                this.deleteSelected()
                break;
        }
    }
    openCamera() {
        this.props.navigation.navigate("camera", { refreshPhotos: this.refreshPhotos })
    }
    async refreshPhotos() {
        let obj = await MediaLibrary.getAssetsAsync({
            first: 100,           // ilość pobranych assetów
            mediaType: 'photo',
            sortBy: 'creationTime'   // typ pobieranych danych, photo jest domyślne
        })

        this.setState({
            data: obj.assets
        })
    }
    changeGrid() {
        if (this.state.grid) this.setState({ grid: false })
        else this.setState({ grid: true })
    }
    getGrid() {
        var styles = StyleSheet.create({
            grid: {
                margin: 1, flex: 1, width: (Dimensions.get('window').width / 4) - 2, height: (Dimensions.get('window').width / 4) - 2
            },
            plain: {
                margin: 5, flex: 1, width: Dimensions.get('window').width - 10, height: Dimensions.get('window').width - 10
            }
        })
        if (this.state.grid) return styles.grid
        else return styles.plain
    }
    goToPhoto(uri, id) {
        this.props.navigation.navigate("photo", { uri: uri, id: id, refreshPhotos: this.refreshPhotos })
        // console.log(uri)
    }
    longClick(uri, id) {
        console.log('LONG CLICK')
        var tab;
        if (this.state.selected.includes(id)) {
            tab = this.state.selected.filter(item => {
                if (item != id) return item
            })
        }
        else {
            tab = this.state.selected;
            tab.push(id);

        }
        this.setState({
            selected: tab
        })
    }
    async deleteSelected() {
        await MediaLibrary.deleteAssetsAsync(this.state.selected);
        this.setState({
            selected: []
        })
        this.refreshPhotos();
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#eeeeee" }} onLayout={this.onLayout}>
                {/* <View style={this.getStyle().container} >
                    <MyButton width="33.3%" text="SIATKA / LISTA" click={this.changeGrid} />
                    <MyButton width="33.3%" text="OTWÓRZ KAMERĘ" click={this.openCamera} />
                    <MyButton width="33.3%" text="USUŃ ZAZNACZONE" click={this.deleteSelected} />
                </View> */}
                <View style={{ flex: 8 }}>
                    <Gallery styles={this.getGrid()} click={this.goToPhoto} longClick={this.longClick} data={this.state.data} refreshPhotos={this.refreshPhotos} />
                </View>
            </View>
        );
    }
    getOrientation() {
        if (this.state.screen.width > this.state.screen.height) {
            return 'LANDSCAPE';
        } else {
            return 'PORTRAIT';
        }
    }
    getStyle() {
        if (this.getOrientation() === 'LANDSCAPE') {
            return landscapeStyles;
        } else {
            return portraitStyles;
        }
    }
    onLayout() {
        this.setState({ screen: Dimensions.get('window') });
    }
}
const portraitStyles = StyleSheet.create({
    container: {
        flex: 1, flexDirection: "row"
    }
});

const landscapeStyles = StyleSheet.create({
    container: {
        flex: 1, flexDirection: "row", marginTop: 15, marginBottom: 10
    }
});


export default Main;
