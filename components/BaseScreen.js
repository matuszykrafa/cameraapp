import React, { Component } from "react";
import { ScreenOrientation } from 'expo';

export class PortraitScreen extends Component {

    constructor(props) {
        super(props);
        ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP)
    }

    componentWillUnmount() {
        // remove subscription when unmount
        ScreenOrientation.unlockAsync();
    }
}