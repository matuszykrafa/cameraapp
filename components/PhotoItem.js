import React, { Component } from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';

class PhotoItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            longPressed: false
        };
        this.longPress = this.longPress.bind(this);
    }
    longPress() {
        this.setState({
            longPressed: !this.state.longPressed
        })
        this.props.longClick(this.props.data.uri, this.props.data.id)
    }
    render() {
        return (
            <TouchableHighlight underlayColor="rgba(100,100,100,0.4)" onPress={() => { this.props.click(this.props.data.uri, this.props.data.id) }} onLongPress={this.longPress}>
                <View>
                    <Image style={this.props.styles} source={{ uri: this.props.data.uri }} />
                    <Text style={styles.text}>{this.props.data.id}</Text>
                    {
                        this.state.longPressed ?

                            <View style={{ backgroundColor: "rgba(200,200,200,0.9)", width: "100%", height: "100%", position: "absolute" }}></View>
                            :
                            null
                    }
                </View>
            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        color: "#eeeeee",
        position: "absolute",
        bottom: 0,
        right: 0,
        margin: 5,

    }
});

export default PhotoItem;
