import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class RadioButton extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.radioPress = this.radioPress.bind(this);
        // console.log(this.props.selected)
    }

    radioPress() {
        this.props.pressFunc(this.props.value);
    }

    render() {
        return (
            <TouchableOpacity onPress={this.radioPress} style={{ flexDirection: "row", alignItems: "center", margin: 15, marginRight: 30, marginLeft: 30 }}>
                <View style={{ width: 30, height: 30, borderRadius: 15, borderColor: "#ff3030", borderWidth: 1, justifyContent: "center", alignItems: "center" }}>
                    {this.props.selected ? <View style={{ width: 25, height: 25, borderRadius: 12.5, backgroundColor: "#ff3030" }}></View> : null}
                </View>
                <Text style={{ color: "white", marginLeft: 10, fontSize: 18 }}> {this.props.text} </Text>
            </TouchableOpacity>
        );
    }
}
