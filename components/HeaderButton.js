import PropTypes from 'prop-types';

import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';

class HeaderButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ width: this.props.width, justifyContent: 'center' }}>
                <TouchableHighlight underlayColor="#aaaaaa" style={styles.button} onPress={this.props.click}>
                    <Text style={styles.text}>{this.props.text}</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

HeaderButton.propTypes = {
    text: PropTypes.string.isRequired,
    click: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#1b5e20",
        padding: 10,
        margin: 10,
    },
    text: {
        textAlign: "center",
        color: "#eeeeee",
        fontSize: 18,
        fontWeight: "500",
    }
})

export default HeaderButton;
