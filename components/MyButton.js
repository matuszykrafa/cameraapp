import PropTypes from 'prop-types';

import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';

class MyButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ width: this.props.width, justifyContent: 'center' }}>
                <TouchableHighlight underlayColor="#aaaaaa" style={styles.button} onPress={this.props.click}>
                    <Text style={styles.text}>{this.props.text}</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

MyButton.propTypes = {
    text: PropTypes.string.isRequired,
    click: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#cccccc",
        padding: 10,
        margin: 10,
    },
    text: {
        textAlign: "center",
        color: "#444444",
        fontSize: 14,
        fontWeight: "500",
    }
})

export default MyButton;
