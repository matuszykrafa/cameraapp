import React, { Component } from 'react';
import { View, Text } from 'react-native';
import RadioButton from './RadioButton';

export default class RadioGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: this.props.startValue
        };
        this.changeSelect = this.changeSelect.bind(this);
    }

    changeSelect(selectValue) {
        this.setState({
            selectedIndex: selectValue
        })
        this.props.onchange(selectValue);
    }

    render() {
        let tab = this.props.radioTab.map((item, index) => {
            return (<RadioButton pressFunc={this.changeSelect} text={item} value={item} selected={item == this.state.selectedIndex} key={index} color={this.props.color} />);
        })
        for (var i = 0; i < this.props.radioTab.length; i++) {
            tab.push()
        }
        return (
            <View style={{ borderTopWidth: 1, borderTopColor: "gray", marginTop: 10 }}>
                <Text style={{ textAlign: "right", color: "white", marginRight: 5 }}>{this.props.groupName}</Text>
                <View>
                    {tab}
                </View>
            </View>
        );
    }
}
