import React, { Component } from 'react';
import { View, Image } from 'react-native';
import HeaderButton from './HeaderButton';
import * as MediaLibrary from "expo-media-library";

class Photo extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "Przeglądaj zdjęcie",
            headerStyle: {
                backgroundColor: "#1b5e20",
            },
            headerTitleStyle: {
                color: "#eeeeee",
                fontSize: 18,
            },
            headerTintColor: '#eeeeee',
            headerRight: <HeaderButton
                text="Usuń" click={async () => {
                    await MediaLibrary.deleteAssetsAsync([navigation.getParam('id')]);
                    navigation.getParam('refreshPhotos')();
                    navigation.goBack();
                }}
            />
        }
    }
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View stlye={{ flex: 1, backgroundColor: '#eeeeee' }}>
                <Image
                    resizeMode={'contain'}
                    style={{ width: "100%", height: "100%" }}
                    source={{ uri: this.props.navigation.state.params.uri }}
                />
            </View>

        );
    }
}

export default Photo;
